DROP DATABASE IF EXISTS ptt_db;
CREATE DATABASE ptt_db;

CREATE USER ptt@localhost IDENTIFIED BY 'mariadb';

GRANT SELECT
   ON ptt_db.*
   TO ptt@localhost;

FLUSH PRIVILEGES;
