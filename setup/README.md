# Setting up the database

To be able to use PTT with a database, we need to create and set up a database.
Therefore, we take the following steps:

#### Install MariaDB to the system
See instructions at 
[the MariaDB download page](https://downloads.mariadb.org/mariadb/repositories/).
  
#### Create a database and user for PTT
Run the SQL script `create.sql` to create a database for the program to use
and create and set up permissions for the user that the program will use.

The database `ptt_db` is created, and user `ptt@localhost` with permissions to
*SELECT* on every table in the database.

#### Set up the database
Run the SQL script `setup.sql` to create the following tables in the database
(primary and foreign keys in **bold**):

###### Activities
* **id (varchar, unique)**
* name (varchar, unique)
* url  (varchar, *optional*)

###### Coordinates
* **id         (int,     auto-gen)**
* tag          (varchar)
* x            (int,     *optional*)
* y            (int,     *optional*)
* n            (decimal, *optional*)
* e            (decimal, *optional*)
* **activity** (varchar, *optional*)
* **type**     (int)

*The* activity *and* type *columns contain foreign keys to* Activity.id
 *and* CoordType.id, *respectively.*

###### CoordTypes
* **id (int,     auto-gen)**
* name (varchar)

###### Persons
* **id (int, auto-gen)**
* first_name (varchar)
* name_prefix (varchar, *optional*)
* last_name (varchar)
* phone (varchar)
* activity (varchar, *optional*)

*The* activity *column contains foreign keys to* Activities.id.

The user `ptt@localhost` is granted permissions to *DELETE, INSERT & UPDATE* on
 the tables `Activities`, `Coordinates` & `Persons` in the `ptt_db` database.
 The `CoordTypes` table is filled with 4 types.

#### Put test data in the database (OPTIONAL)
Run the SQL script `testdata.sql` to insert some test data into the tables.

Creates some useful Activities, Coordinates and Persons.