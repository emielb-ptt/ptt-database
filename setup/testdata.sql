USE ptt_db;

INSERT INTO Activities (id, name, url)
    VALUES
        ( 'A', 'Alfa',     'http://www.jotihunt.net/alpha.html' ),
        ( 'B', 'Bravo',    'http://www.jotihunt.net/bravo.html' ),
        ( 'C', 'Charlie',  'http://www.jotihunt.net/charlie.html' ),
        ( 'D', 'Delta',    'http://www.jotihunt.net/delta.html' ),
        ( 'E', 'Echo',     'http://www.jotihunt.net/echo.html' ),
        ( 'F', 'Foxtrot',  'http://www.jotihunt.net/foxtrot.html' ),
        ( 'H', 'Hints',    'http://www.jotihunt.net' ),
        ( 'K', 'Creative', 'http://www.jotihunt.net' ),
        ( 'L', 'Away',     NULL ),
        ( 'P', 'Photos',   NULL ),
        ( 'S', 'Asleep',   'https://en.wikipedia.org/wiki/Counting_sheep' );

INSERT INTO Coordinates (tag, x, y, n, e, activity, type)
    VALUES
        ( 'De Geuzen Arnhem', 188588, 444444, NULL, NULL, NULL, 1 ),
        ( 'Sabelspoort, Arnhem', NULL, NULL, 51.97723, 5.90938, 'P', 4 ),
        ( 'Scouting Duiven', 199345, 439705, NULL, NULL, NULL, 1 ),
        ( '2017-05-05T12:07:15.375Z', NULL, NULL, 51.91516, 5.95665, 'F', 3 ),
        ( 'Gerritsfles', NULL, NULL, 52.16729, 5.82575, 'P', 4 ),
        ( '2017-05-05T12:54:18.815Z', 170662, 477839, NULL, NULL, 'B', 2 ),
        ( '2017-05-05T12:55:43.067Z', 159457, 434825, NULL, NULL, 'A', 2 ),
        ( '2017-05-05T12:01:37.491Z', 176851, 444830, NULL, NULL, 'D', 2 );

INSERT INTO Persons (first_name, name_prefix, last_name, phone, activity)
    VALUES
        ( 'Sjaak',     'de',      'Vries',    '0612345678', 'A' ),
        ( 'Henk',      'van der', 'Steen',    '0612341234', 'B' ),
        ( 'Jan',       NULL,      'Klaassen', '0631415926', 'C' ),
        ( 'Truus',     'de',      'Mier',     '0613467950', 'P' ),
        ( 'Annemarie', NULL,      'Koekoek',  '0611111111', 'L' );
