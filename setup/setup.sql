USE ptt_db;

CREATE TABLE IF NOT EXISTS Activities
(
    id      varchar(1)   NOT NULL UNIQUE,
    name    varchar(64)  NOT NULL UNIQUE,
    url     varchar(128) NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS CoordTypes
(
    id      int         NOT NULL AUTO_INCREMENT,
    name    varchar(8)  NOT NULL UNIQUE,
    PRIMARY KEY (id)
);

INSERT INTO CoordTypes (name)
    VALUES
        ( 'Group' ),
        ( 'Hint'  ),
        ( 'Hunt'  ),
        ( 'Photo' );

CREATE TABLE IF NOT EXISTS Coordinates
(
    id          int             NOT NULL AUTO_INCREMENT,
    tag         varchar(64)     NOT NULL,
    x           int             NULL,
    y           int             NULL,
    n           decimal(9,6)    NULL,
    e           decimal(9,6)    NULL,
    activity    varchar(1)      NULL,
    type        int             NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (activity) REFERENCES Activities(id),
    FOREIGN KEY (type)     REFERENCES CoordTypes(id)
);

CREATE TABLE IF NOT EXISTS Persons
(
    id          int         NOT NULL AUTO_INCREMENT,
    first_name  varchar(32) NOT NULL,
    name_prefix varchar(16) NULL,
    last_name   varchar(64) NOT NULL,
    phone       varchar(16) NOT NULL,
    activity    varchar(1)  NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (activity) REFERENCES Activities(id)
);

GRANT DELETE,
      INSERT,
      UPDATE
   ON ptt_db.Activities
   TO ptt@localhost;

GRANT DELETE,
      INSERT,
      UPDATE
   ON ptt_db.Coordinates
   TO ptt@localhost;

GRANT DELETE,
      INSERT,
      UPDATE
   ON ptt_db.Persons
   TO ptt@localhost;

FLUSH PRIVILEGES;
