# Clearing the database

There are several level to which we can clear the database with the scripts
 in this folder. None of these commands can be undone,
 so think carefully before executing any of them.

#### Remove data
Run the SQL script `data.sql` to delete all data.
All entries are removed from the `Activities`,
`Coordinates` and `Persons` tables.

#### Drop tables
Run the SQL script `tables.sql` to drop all tables.
The `Activities`, `Coordinates`, `CoordTypes`
 and `Persons` tables are dropped.
The special permissions on `Activities`, `Coordinates`
 and `Persons` tables are revoked.

#### Drop everything
Run the SQL script `all.sql` to drop all PTT-related data
 from the MariaDB server.
The entire `ptt_db` database is dropped,
 as well as the `ptt@localhost` user.