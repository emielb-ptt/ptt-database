USE ptt_db;

DROP TABLE IF EXISTS Persons;
DROP TABLE IF EXISTS Coordinates;
DROP TABLE IF EXISTS CoordTypes;
DROP TABLE IF EXISTS Activities;

REVOKE DELETE,
       INSERT,
       UPDATE
   ON   ptt_db.Activities
   FROM ptt@localhost;

REVOKE DELETE,
       INSERT,
       UPDATE
   ON   ptt_db.Coordinates
   FROM ptt@localhost;

REVOKE DELETE,
       INSERT,
       UPDATE
   ON   ptt_db.Persons
   FROM ptt@localhost;

FLUSH PRIVILEGES;
