# `ptt-database` repo

The Presence Tracking Tool (PTT) is a tool which can be used
to track the occupation of people across different activities.
The tool has been developed with the [JOTI-hunt](http://www.jotihunt.nl) in
mind, but its use is not limited to that.

Ptt-database is a MariaDB type relational database storage, and one of the
three composing parts of the Presence Tracking Tool, which consists of a
storage, a server and a cross-platform mobile app for Android/iOS.

This repository contains instructions and SQL scripts for setting up and
tearing down the MariaDB relational database that is used for persisting PTT
data.
See the README in the `setup` directory for instructions on installing MariaDB,
setting up the database, creating tables, and inserting some test entries.
See the README in the `clear` directory for instructions on deleting data,
dropping tables or dropping the entire database.  

### Running scripts
> The SQL scripts in the `setup` and `clear` directories need to be executed
> by the root user of the MariaDB server.
>
> **Mind that all SQL scripts in the `clear` directory are destructive and
> irreversible.**

To run a script, the Bash script dbutil can be used.
It takes one or two arguments.

With one argument, it runs all SQL scripts in the directory whose name is
given, e.g.:

    ./dbutil setup

This is useful for the setup scripts, but not so much for the clear scripts,
which are best run by themselves. With two arguments, the Bash script runs
from the given directory the given script (no extension), e.g.:

    ./dbutil clear data

The scripts can be run manually by `cat`ing them, and piping them into
`mysql -u root -p`. This is exactely what the Bash script does, but by manually
running them, you have a chance to add options or use a different account.

### Entity-Relation diagram
![ER-diagram](doc/ER diagram.png)

Diagram made at [draw.io](https://www.draw.io).
